# Tables

## Use natural primary keys

If the data in a table is going to naturally a set of columns that uniquely and
directly identify the rows, then that set of columns is naturally suitable to be
a primary key.

If a table does not have such a set of columns, try not to force a surrogate
primary key (e.g., autoincrementing integer keys) on it. A table without a
primary key is preferable to a table with a forced, unrelated, or
not-replicatable primary key.

If you must use a primary key on a table whose data is not naturally
identifiable, change the data model to include auto-generateable identifier keys
that play nice with replication (e.g.: random UUIDs).

## Make tables replicatable

Logical replication of UPDATEs & DELETEs requires the DB know how to correctly
identify rows in replicas.
[Postgres](https://www.postgresql.org/docs/current/static/logical-replication-publication.html)
does this using [REPLICA
IDENTITY](https://www.postgresql.org/docs/current/static/sql-altertable.html#SQL-CREATETABLE-REPLICA-IDENTITY).

A PK on a table is automatically sufficient for this, but for tables that do not
have PKs, set the REPLICA IDENTITY appropriately
