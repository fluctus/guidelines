# Git

## Use branches

Easy organization in just 41 bytes. Read [Chap. 3: Git
Branching](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
of the git book.

You're using branches wrong if two unrelated or independent features or fixes
are being worked on in the same branch.

In general, you should make a branch each for every feature you work on and every
bug you're trying to fix. The branches can be merged into a release branch when
they're ready and pass review. (Small hotfixes without a new branch are okay, but
only for the smallest, superbly uncomplicated changes.)

## Commit early, commit often

A commit is the smallest meaningful unit of change, not an excuse to dump code.
Separate your changes to the code into logical pieces and commit each piece
individually. If a feature is complex enough to require multiple, disparate
changes, then each change shall be one or more commits.

There are many benefits of this practice, in addition to the organizational
cleanliness of it:

- Reviewing lesser code is always easier than reviewing larger
- Reverting/reworking any small portion is easier if that portion is a commit
  of its own
- Commit messages can be more accurate and less boring

## Know how to write useful commit messages

A commit message should describe the change the commit contains, fully and
accurately. Commit messages almost serve as documentation in the long run; and
should fulfil the role of primary documentation during review and integration.

A git commit message contains two sections: a short one-line summary, and an
(as large as required) description.

The summary should succintly describe the changes — this gets harder to do in
one line if you don't follow [the 'commit early, commit often'
rule](#commit-early-commit-often). You should try to word is so that it
completes the sentence: *When applied, this commit will ...*

The description can be as large (or as small, or even nonexistent) as needed;
it can even (and usually does) contain paragraphs. The description should
describe the change in detail — any detail you seem fit.

Wrap the description at 72 characters per line, and limit the summary to 72
characters.

## Always check what you are about to commit

- Don't `git add .` or `git add *` unless you have checked exactly what will
  get added. (`git status` is your friend).
- Run `git diff --staged` to see what will get committed, and go through the
  changes and verify them. (If you have `$EDITOR` set, check out `git commit
  -v`)
- Use `git add -p` if you have changes you don't want to commit right now. `git
  add -p` allows you to edit the contents before staging them, without having
  to edit your source files.

----

# General Programming

## Don't commit commented-out code

If you're commenting-out code in a commit, you're likely doing something wrong.

Code that needs replacing should be removed instead of being commented-out;
if/when the old code is needed, it can be checked out from git.

If you're commenting-out code to disable it, consider if placing it within
a conditional would be better. If you're having trouble seeing it that way, you
may be confusing configuration with code.

If you still think you have legitimate reason to commit commented-out code,
supply an explanation comment alongwith the commented out code.


# DB Programming

See [DB.md](DB.md)
