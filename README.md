Collected Coding Guidelines. It's always a good idea to follow industry best
practices, and this is not an epic document trying to document every aspect of
software development, so don't restrict yourself to practices mentioned here.

See [GUIDELINES.md](GUIDELINES.md)
