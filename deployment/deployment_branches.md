Deployment branches are branches that exist only for the sake of a specific
deployment or installation. They are different from release/development
branches (e.g.: `dev`, `master`) and topic branches (e.g.: `ft/...`, `fix/...`
etc.) in the following ways:
1. `dev` and topic branches are pulled and pushed, but deployment branches are
   not. There is no branch named `staging` or `prod` on GitLab. Deployment
   branches exist only at the point of installation.
2. Commits that have been merged into `dev` cannot be rewritten (i.e., one
   cannot `git rebase` or `git commit --amend` commits that are already in
   `dev`). No such restrictions apply to deployment branches. In fact,
   deployment branches do not even need to be retained. They can be
   deleted/replaced/recreated without issue.

Deployment branches are needed only as a convenient way to have a **local,
temporary** merge of multiple branches that need to be deployed.

----

Now, let's say we have just made a new deployment on staging that includes the
latest `dev`, and `ft/A` and `ft/B`:

```sh
$ git fetch origin
$ git checkout -b staging origin/dev # Create a new branch named 'staging' at tip of 'origin/dev' and checkout 'staging'
$ git merge origin/ft/A
$ git merge origin/ft/B
```

A few hours later, let's say some commits have been added to `ft/A`, and some
other commits to `dev`. Deploying the new `dev` and `ft/A` is simple enough —
just merge those two branches into `staging`!:

```sh
$ git fetch origin
$ git merge origin/dev
$ git merge origin/ft/A
```

But if then some commits that were in `ft/B` (and were merged into `staging`)
got rewritten, and `ft/B` was pushed to`origin` with a `git push -f`, deploying
the new `ft/B` cannot be done by just merging the branch into `staging`.

Or if `ft/A` got merged into `dev`, then the next time `dev` is merged into
`staging`, it will also bring in `ft/A`; but `ft/A` was already in `staging`
and this just adds clutter to the log of `staging`.

So it is preferable to keep deployment branches short-lived and recreate them
on every deployment. Something like:

```sh
$ git checkout -B staging-old staging # Create (forcibly) new branch named 'staging-old' at 'staging' and checkout to 'staging-old'
$ git branch -D staging # Delete (forcibly ) branch named 'staging'
$ git checkout -b staging origin/dev
$ git merge ...
$ git merge ...
$ ...
$ git branch -D staging-old
```

Doing this on every deployment would, of course, keep things clean and help
ensure really-old branches don't silently sit around on staging without being
merged into `dev`.

The only reasonable cases where simply merging a branch into `staging` instead
of doing a full re-creation of `staging` is acceptable are when:
1. The branch hasn't been merged before into the current `staging`; or
2. The branch has not been rewritten and has had only new commits since the
   last time it was merged to `staging`; and
3. Those new commits do not include any merge commits from other branches that
   also have been merged into staging.

In other words, please do a full re-creation when:
1. A branch has been merged into `dev` and you're deploying that new `dev`;
2. You're deploying updates to a topic branch and you're not sure if the
   updates contain just new commits (i.e., additions) instead of any rewrites
   (topic branches are allowed to be rewritten, `dev` is not); or
3. You're undeploying a branch (i.e., removing a branch from the deployment).
